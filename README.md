# Prueba técnica Experta ART 

Se utilizó Docker y Docker Compose para la configuración del proyecto.

| Nombre         | Version |
|----------------|---------|
| Python         | 3.6.6   |
| Flask          | 1.0.2   |
| requests       | 2.19.1  |
| Unidecode      | 1.0.22  |
| gunicorn       | 19.9.0  |

**[Docker image para servidor FTP](https://github.com/stilliard/docker-pure-ftpd)**

## Configuración:

Renombrar `ftp_config.example.env` a `ftp_config.env`.
Agregar las variables necesarias. Un ejemplo válido sería:

```dotenv
PUBLICHOST=ftpd_server
FTP_USER_NAME=ftp
FTP_USER_PASS=test@1234
FTP_USER_HOME=/home/ftp
```

## Correr el proyecto:
```bash
$ docker-compose up  # para iniciar el web service. 
$ ./main.py  # Consumir el web service usando datos.txt y lista_negra.txt
```
