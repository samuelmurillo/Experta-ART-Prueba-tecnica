#!/usr/bin/python3

import requests
import unidecode

# URL donde está el web service
URL = 'http://127.0.0.1:8000'


def parse_country_list(filename: str) -> list:
    """
    Función de ayuda para limpiar lineas de un archivo y leerlo.
    :param filename: path del archivo.
    :return: lista con lineas del archivo.
    """
    with open(filename) as f:
        content = f.readlines()
        # Remover espacios, fin de linea, caracteres especiales. Usar solo mayusculas.
        countries = [unidecode.unidecode(x.strip().lower()) for x in content]
        return countries


if __name__ == '__main__':
    data = parse_country_list("./datos.txt")
    blacklist = parse_country_list("./lista_negra.txt")

    # Filtar paises. Paises que esten en `data` y no en `lista_negra` seran procesados.
    country_list = [c for c in data if c not in blacklist]

    for country in country_list:
        # Consumir web service
        r = requests.get(f"{URL}?country={country}")
