FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip setuptools

COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

WORKDIR /src
ADD . /src
