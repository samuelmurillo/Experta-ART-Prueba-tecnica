import logging.handlers
import os
import pathlib
from ftplib import FTP
from logging import Formatter

import requests
from flask import Flask, request, render_template

FTP_HOST = os.getenv("PUBLICHOST")
FTP_USER_NAME = os.getenv("FTP_USER_NAME")
FTP_USER_PASS = os.getenv("FTP_USER_PASS")

app = Flask(__name__)

# Configurar logger para stdout y archivo.
gunicorn_logger = logging.getLogger('gunicorn.error')
app.logger.handlers = gunicorn_logger.handlers
app.logger.setLevel(gunicorn_logger.level)

handler = logging.handlers.RotatingFileHandler(
    'app.log',
    maxBytes=1024 * 1024
)
handler.setFormatter(Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))

handler.setLevel(logging.INFO)
app.logger.addHandler(handler)


@app.route('/')
def index():
    """
    Endpoint principal del web service. Espera un parametro ?country. Ej: ?country=argentina.
    Hace una busqueda en http://services.groupkt.com/country/search, toma el primer valor encontrado (si existe),
    se escribe en un archivo HTML en formato de tabla. Este archivo es subido a un servidor FTP y retornado como response.
    """
    query = request.args.get("country")
    if query is not None and query != "":
        app.logger.info(f"Querying http://services.groupkt.com/country/search?text={query}")
        r = requests.get(f"http://services.groupkt.com/country/search?text={query}")
        response = r.json()
        results = response["RestResponse"]["result"]

        if len(results) > 0:
            app.logger.info(f"Successful query to http://services.groupkt.com/country/search?text={query}")
            result = results[0]
            country_data = {
                "name": result["name"],
                "alpha2_code": result["alpha2_code"],
                "alpha3_code": result["alpha3_code"]
            }

            app.logger.info(f"Writing response to ./responses/{result['name']}.html")
            html_response = render_template('country.html', country=country_data)
            pathlib.Path('./responses/').mkdir(parents=True, exist_ok=True)
            with open(f"./responses/{result['name']}.html", "w") as file:
                file.write(html_response)
                app.logger.info(f"Wrote response to ./responses/{result['name']}.html")

            app.logger.info(f"Uploading ./responses/{result['name']}.html to FTP {FTP_HOST}")
            with open(f"./responses/{result['name']}.html", "rb") as file:
                with FTP(FTP_HOST, FTP_USER_NAME, FTP_USER_PASS) as ftp:
                    ftp.storbinary(f"STOR {result['name']}.html", file)

            app.logger.info(f"Uploaded ./responses/{result['name']}.html to FTP {FTP_HOST}")

            return html_response
        else:
            app.logger.warning(f"http://services.groupkt.com/country/search?text={query} did not return any results")
            return render_template('error.html', error_msg="No se encotraron registros"), 404
    else:
        app.logger.warning(f"Empty query received")
        return render_template('error.html', error_msg=u"No se recibi\u00F3 el par\u00E1metro 'country'"), 400


if __name__ == "__main__":
    app.run(host='0.0.0.0')
